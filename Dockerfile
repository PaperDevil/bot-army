FROM python:3.9-slim as builder

WORKDIR /

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN apt update && \
    apt install nano && \
    apt install -y --no-install-recommends gcc

RUN python -m venv /opt/venv
ENV PATH="/opt/venv/bin:$PATH"

COPY requirements.txt .
RUN --mount=type=cache,target=/root/.cache/pip \
    pip install -r requirements.txt

FROM python:3.9-slim

COPY --from=builder /opt/venv /opt/venv

WORKDIR /
COPY . .

ENV PATH="/opt/venv/bin:$PATH"
ENV PYTHONPATH="/opt/venv/bin:$PYTHONPATH"