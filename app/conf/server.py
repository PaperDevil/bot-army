from app.external.utils.config import ConfigUtils


DEBUG = ConfigUtils.env('DEBUG', bool)
TITLE_API = ConfigUtils.env('TITLE_API', str)
DESCRIPTION_API = ''
VERSION_API = ConfigUtils.env('VERSION_API', str)

HTTPS_HOST_ADDRESS = ConfigUtils.env('HTTPS_HOST_ADDRESS', str)
