from app.schema.models import Bot


class BotService:
    model = Bot

    @staticmethod
    async def add(bot: Bot) -> Bot:
        await bot.save()
        return bot

    @staticmethod
    async def get_by_id(_id: int) -> Bot:
        bot = await Bot.get(id=_id)
        return bot
