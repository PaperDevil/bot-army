from fastapi import APIRouter, Request, Response

from app.external.utils.classify import classify
from app.schema.models import Bot


open_router = APIRouter(prefix='/open')


@open_router.post('/answer/{botname}')
async def get_answer(botname: str, request: Request):
    payload = await request.json()
    bot = await Bot.objects.get(botname=botname)
    return Response({
        'out_text': classify(url=bot.classifier, text=payload.get('in_text', '')),
    }, status_code=200)
