from fastapi import APIRouter, Request
from aiogram import Bot as TelegramBot
from aiogram import types

from app.internal.web.telegram.utils.dispatcher import get_dispatcher
from app.schema.models import Bot

telegram_router = bots_router = APIRouter(prefix='/tg')


@telegram_router.post('/receive/{token}')
async def receive_telegram_update(token: str, request: Request):
    payload = await request.json()
    bot = await Bot.objects.get(telegram_token=token)
    TelegramBot.set_current(bot.telegram_bot)
    await get_dispatcher(bot).process_update(
        update=types.Update(**payload)
    )
