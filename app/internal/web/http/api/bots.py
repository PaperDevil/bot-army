from fastapi import APIRouter
from fastapi.exceptions import HTTPException
from typing import List

from app.conf.server import HTTPS_HOST_ADDRESS
from app.schema.models import Bot

bots_router = APIRouter(prefix='/bots')


@bots_router.get('/all', response_model=List[Bot])
async def get_all_bots():
    bots = await Bot.objects.all()
    return bots


@bots_router.post('/delete_webhook', response_model=bool)
async def delete_webhook(bot_id: int):
    bot = await Bot.objects.get_or_none(id=bot_id)
    if not bot:
        raise HTTPException(status_code=404)
    await bot.telegram_bot.delete_webhook()
    return True


@bots_router.post('/set_webhook', response_model=bool)
async def set_webhook(bot_id: int):
    bot = await Bot.objects.get_or_none(id=bot_id)
    if not bot:
        raise HTTPException(status_code=404)
    await bot.telegram_bot.set_webhook(
        HTTPS_HOST_ADDRESS + f'/v1/tg/receive/{bot.telegram_token}',
        drop_pending_updates=True
    )
    return True
