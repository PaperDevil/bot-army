from fastapi import APIRouter

from app.external.entities.exception_response import ExceptionResponse
from app.internal.web.http.api.bots import bots_router
from app.internal.web.http.api.open import open_router
from app.internal.web.http.api.telegram import telegram_router
from app.internal.web.http.api.vk import vk_router

general_router = APIRouter(prefix='/v1', responses={400: {'model': ExceptionResponse},
                                                    500: {'model': ExceptionResponse}})

general_router.include_router(bots_router)
general_router.include_router(telegram_router)
general_router.include_router(vk_router)
general_router.include_router(open_router)
