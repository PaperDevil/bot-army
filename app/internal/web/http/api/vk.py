import random

from fastapi import APIRouter, Request, Response

from app.external.utils.classify import classify
from app.schema.models import Bot

vk_router = APIRouter(prefix='/vk')


@vk_router.post('/receive/{botname}')
async def receive_vk_update(botname: str, request: Request):
    payload = await request.json()
    bot = await Bot.objects.get(botname=botname)
    if payload['type'] == 'confirmation':
        return Response(content=bot.vk_confirmation)
    elif payload['type'] == 'message_new':
        message = payload['object']['message']
        await bot.vk_bot.request('messages.send', {
            'user_ids': message['from_id'],
            'message': classify(url=bot.classifier, text=message['text']),
            'random_id': random.randint(0, 999999999)
        })
