from aiogram import Dispatcher

from app.external.utils.handlers import include_handlers
from app.internal.web.telegram.handlers.general import general_handler


def get_dispatcher(bot) -> Dispatcher:
    return include_handlers(general_handler, Dispatcher(bot.telegram_bot))
