from aiogram import types

from app.external.utils.classify import classify
from app.schema.models import Bot


async def process_message(event: types.Message):
    bot = await Bot.objects.get(telegram_token=event.bot.get_current()._token)
    await event.answer(classify(url=bot.classifier, text=event.text))
