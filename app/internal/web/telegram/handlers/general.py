from app.internal.web.telegram.handlers.text import process_message

general_handler: dict = {
    'message': [
        {'callback': process_message}
    ]
}
