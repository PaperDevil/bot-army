import sys

from fastapi import FastAPI
from loguru import logger

from app.conf.db import REDIS_HOST, REDIS_PORT, REDIS_PASSWORD
from app.conf.server import DEBUG, TITLE_API, DESCRIPTION_API, VERSION_API
from app.internal.drivers.async_pg import AsyncPg
from app.internal.drivers.cache_driver import CacheDriver
from app.internal.web.http.api.general import general_router


class FastAPIServer:
    @staticmethod
    def get_app() -> FastAPI:
        logger.add(sys.stderr, level='INFO')

        app = FastAPI(
            debug=DEBUG,
            title=TITLE_API,
            description=DESCRIPTION_API,
            version=VERSION_API,
            docs_url='/docs',
            openapi_url='/openapi.json'
        )

        app.include_router(general_router)

        @app.on_event('startup')
        async def init_primary_db():
            app.state.database = await AsyncPg.init_primary_db()
            # await CacheDriver.init_redis_connection(REDIS_HOST, REDIS_PORT, REDIS_PASSWORD)

        @app.on_event('shutdown')
        async def close_primary_db():
            await AsyncPg.close_primary_pool_db()
            # await CacheDriver.close_redis_connection()

        return app
