import requests


def classify(url: str, text: str) -> str:
    if not url:
        return "Этот бот ещё не готов к работе. Обратитесь к службе технической поддержки бота в описании."
    r = requests.post(url, json={'text': text})
    if r.status_code == 200 and r.json()['answer'] != 'Trash class':
        return r.json()['answer']
    return "Боюсь я не понимаю чего вы хотите. Попробуйте снова."
