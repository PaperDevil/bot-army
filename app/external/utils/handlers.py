from aiogram import Dispatcher


def include_handlers(handlers: dict[str:list], dispatcher: Dispatcher) -> Dispatcher:
    for group_name, group in handlers.items():
        if group_name == 'message':
            for handler in group:
                dispatcher.register_message_handler(
                    callback=handler.get('callback'),
                    commands=handler.get('commands')
                )
        elif group_name == 'callback':
            for handler in group:
                dispatcher.register_callback_query_handler(
                    callback=handler.get('callback'),
                    text=handler.get('text')
                )
        else:
            raise TypeError(f"Type of handler not supported! {group}")
    return dispatcher
