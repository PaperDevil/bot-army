from app.conf.db import DB_PORT, DB_HOST, DB_PASSWORD, DB_PRIMARY_NAME, DB_USER


def get_url():
    return f"postgresql://{DB_USER}:{DB_PASSWORD}@{DB_HOST}:{DB_PORT}/{DB_PRIMARY_NAME}"