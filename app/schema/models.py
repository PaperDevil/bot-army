from datetime import datetime

import ormar
from vkbottle.api import API as VkAPI
from aiogram import Bot as TelegramBot

from app.schema.base import metadata, database


class BaseTable(ormar.Model):
    class Meta:
        abstract = True
        metadata = metadata
        database = database

    id: int = ormar.Integer(primary_key=True)
    created_at: datetime = ormar.DateTime(default=datetime.now)
    updated_at: datetime = ormar.DateTime(default=datetime.now)


class Bot(BaseTable):
    class Meta(ormar.ModelMeta):
        tablename = "bot"

    botname: str = ormar.String(max_length=64)
    telegram_token: str = ormar.String(max_length=512, nullable=True)
    vk_token: str = ormar.String(max_length=512, nullable=True)
    vk_confirmation: str = ormar.String(max_length=24, nullable=True)
    classifier: str = ormar.String(max_length=255, nullable=True)

    @property
    def telegram_bot(self) -> TelegramBot:
        return TelegramBot(self.telegram_token)

    @property
    def vk_bot(self) -> VkAPI:
        return VkAPI(token=self.vk_token, ignore_errors=True)
